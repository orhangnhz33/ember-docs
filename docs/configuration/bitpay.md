# Setting up BitPay

::: tip
Make sure you are logged in with the account using which you purchased Ember. Other users cannot access the store configuration even if they have administrative permissions.
:::

- Navigate to `Admin`>`Store` and select the `Settings` tab.
  - Select a currency that is supported by BitPay.
  - Enter the `API Token` from your [BitPay dashboard](https://bitpay.com/dashboard/merchant/api-tokens).
- Click on the `Save` button.
