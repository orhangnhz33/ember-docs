# Enabling the cache

Configuring the cache significantly reduces the amount of requests that have to be sent to Steam's API and queries made to the database server and game servers, thus increasing performance when generating pages. The configuration is done in the `config.php` file.

Caching can be handled using files (simpler), [Memcached](https://memcached.org) or [Redis](https://redis.io) (slightly more performant). By default Ember utilizes file cache.

### Example configuration
```php
'cache' => [
    'cache.default' => 'file', // 'file', 'redis', 'memcached' or 'array' (no cache)
    'database.redis' => [
        'cluster' => false,
        'default' => [
            'host' => '127.0.0.1',
            'password' => null,
            'port' => 6379,
            'database' => 0
        ]
    ],
    'cache.stores.memcached' => [
        'servers' => [
            [
                'host'   => '127.0.0.1',
                'port'   => 11211,
                'weight' => 100
            ]
        ]
    ]
],
```
