# Adding a store package

- Navigate to `Admin`>`Store packages`, select a server and click on the `+` icon in the top left of the store package manager.
- Input fields will appear. Populate the fields with the details of the package.
  - Cost for the package in credits (100 credits = 1 unit of the selected currency).
  - On the `Actions` section you can configure the actions that will be run when the package is applied, for instance assigning a role and executing custom Lua.

::: tip
To make packages redeemable on multiple servers, assign the same token for the servers in the integration plugin/addon configuration (while retaining unique tokens in **Admin** > **Servers**).
:::

### Example configuration
![](../media/screenshots/store_package_manager.jpg)

## Package actions
Below is a partial overview of the package actions available.

### Role
- The specified role will be assigned to the user on the web instance on purchase.
- A group with the name of the role's in-game equivalent will be assigned to the user in-game.
  - For Garry's Mod a [CAMI](https://github.com/glua/CAMI) group will be applied to support multiple administration mods, including [ULib](https://github.com/TeamUlysses/ulib)/[ULX](https://github.com/TeamUlysses/ulx).
  - For Rust an [Oxide](https://umod.org/documentation/plugins/permissions) group will be applied.
- If the package is set to expire, the role and groups will be revoked upon next server connection after expiry.

### Garry's Mod
#### Custom Lua
The store package configuration includes a field for custom Lua. Lua is the programming language used in Garry's Mod addons. [General usage instructions can be found here.](https://wiki.garrysmod.com/page/Category:Beginner_Tutorials) With custom Lua you can integrate Ember with other addons that aren't supported out of the box.

In the custom Lua editor you can access two predefined variables:
- `ply`, which refers to the player entity of the player whom the package is being applied for
- `ember.package[ply:SteamID64()]`, which is a table that contains the attributes of the package being applied
  - attributes include `name` and `cost`
#### Permanent weapons
Permanent weapons that are given to the player on each [spawn](https://wiki.facepunch.com/gmod/GM:PlayerSpawn) and [team change](https://wiki.facepunch.com/gmod/GM:OnPlayerChangedTeam) during the validity of the package. Cannot be [dropped](https://darkrp.miraheze.org/wiki/Hooks/Server/canDropWeapon) in DarkRP when `GM.Config.dropspawnedweapons` is set to `false`.

### Rust
#### Commands & expiry commands
Console commands that will be run once on the server when the player first connects after purchase/expiry of the package. For example `inventory.giveto {ply.steamid} syringe.medical 5`.

The following variables are available:
- `{ply.steamid}`
- `{ply.name}`
- `{package.name}`
- `{package.cost}`
