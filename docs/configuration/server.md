# Adding a server

- Navigate to `Admin`>`Servers` and click on the `+` icon on the top left of the server manager.
- Input fields will appear. Populate the fields with the details of the server.
  - Enter a descriptive name for the server, such as `DarkRP`.
  - Enter the public IP address or domain of the server.
  - Enter the server's game port and query port.
  - Specify the game the server is running.
  - Optionally configure a custom cover image for the landing page server card.
  - Generate a token for the server.

## Remote console
If the RCON configuration options are filled, a remote console component will appear below the server manager. Ember will also utilize the RCON connection behind the scenes to instantly assign store package roles in-game and enforce server-scoped bans issued from the web interface.

The connection can be tested by sending a command (for example `status`).

## Example configuration
![](../media/screenshots/server_manager.jpg)
