# Setting up Stripe

## Ember configuration
::: tip
Make sure you are logged in with the account using which you purchased Ember. Other users cannot access the store configuration even if they have administrative permissions.
:::

- Navigate to `Admin`>`Store` and select the `Settings` tab.
  - Select a currency that is set up on your Stripe account.
  - Enter the `Publishable key` and `Secret key` from your [Stripe dashboard](https://dashboard.stripe.com/apikeys).
- Click on the `Save` button.

## Stripe configuration
- While still in the settings page, copy the Webhook URL.
- Browse to the [Webhooks page](https://dashboard.stripe.com/webhooks) in Stripe's dashboard. Hit `Add endpoint` and enter the URL you copied earlier. Choose `checkout.session.completed` from the `Events to send` dropdown.
![](../media/screenshots/stripe_webhooks.jpg)
- Retrieve the Webhook `Signing secret` from [Stripe's dashboard](https://dashboard.stripe.com/webhooks) and enter it in Ember's configuration.
![](../media/screenshots/stripe_webhook_signing_secret.jpg)