# Webhooks

Webhooks are user-defined HTTP callbacks. They are triggered by database model (an object representing a table of data) lifecycle events (`created`, `updated`, `saved` and `deleted`).

Webhooks can be defined in **Admin** > **General** > **Webhooks**.

::: v-pre
# Expressions
Expressions can be used in the condition, the URL and the request body.
## Attributes
Database model attribute names can be determined by inspecting the database schema (table column names) and model definitions.
* Attributes can be embedded in an expression using two pairs of curly brackets (`{{}}`).
* Attributes can change their value during an event. The previous value is accessible by appending `_original` to the attribute name.
* Subattributes can be accessed with the `.` operator.

Examples: `{{ id }}`, `{{ user.name }}`, `{{ thread.board.name }}` and `{{ thread.board_original.name }}`.
## Filters
Filters can be applied to an expression using the pipe operator (`|`). Some filters have arguments that can be passed in parentheses `()`. Examples: `{{ name | upper }}`, `{{ name | lower | capitalize }}` and `{{ name | truncate(5) }}`.

The following filters are available:
* `truncate`
* `lower`
* `upper`
* `capitalize`.

# Condition
The condition is a series of boolean and relational expressions that are evaluated to determine whether to trigger the webhook.
In addition to the expressions above, the condition supports expressions with the following operators:
* relational operators `==` and `!=`
* logical operator `&&`.

# Examples
## Send new forum posts to Discord
* **Model**: `ForumPost`
* **Event**: Created
* **Condition**: `{{ thread.bid }} == 1` (only include posts from the board with the ID `1`)
* **URL**: obtain from [Discord **Server Settings**](https://support.discord.com/hc/en-us/articles/228383668-Intro-to-Webhooks)
* **Body**:
```json
{
	"embeds": [
		{
			"title": "{{ thread.topic }}",
			"url": "https://example.com/forums/posts/{{pid}}",
			"description": "{{ content | truncate(150) }}",
			"footer": {
				"text": "Posted by {{ user.name }}",
				"icon_url": "{{ user.steam.avatar }}"
			}
		}
	]
}
```
:::
