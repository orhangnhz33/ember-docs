# Modules
Ember comes with a module system that allows for creating new dynamic pages & functionalities.

## Examples
There are a couple of example modules available to demonstrate the basic structure & functionality of a module. 
* [Generic example module](https://gitlab.com/kekalainen/ember-module-example)
* [Sentry integration module](https://gitlab.com/embercms/sentry)

## Helper functions
* Registering a module: `Module::registerModule($array)`. For example
```php
Module::registerModule([
    'name' => 'Example module',
    'identifier' => 'example',
    'version' => '1.0.0',
    'description' => 'An example module for Ember.'
]);
```
* Registering a controller: `Module::registerController($class, $name)`
* Registering an event listener: `Module::registerEventListener($event, $listener)`
* Creating a navbar link: `Module::addNavbarLink($link)`. For example
```php
Module::addNavbarLink([
    'icon' => 'fas fa-box',
    'name' => 'Example',
    'url' => '/example',
    'admin_dropdown' => false
]);
```

## Events
Event listeners provide a way to listen for changes to Ember's models and act on them.
* available events can be found in `app/Events`
* the example module contains an [event listener](https://gitlab.com/kekalainen/ember-module-example/-/blob/master/modules/example/module/Listeners/StoreCreditSaving.php)

Visit [Laravel's documentation](https://laravel.com/docs/7.x/events) for more information.

## Assets
Module assets (JavaScript & CSS files) located relative to the module's `public` directory can be loaded from the `/modules/{identifier}` route by passing the file path to the `f` URL parameter, for example `/modules/example?f=/js/app.js`.

The `moduleasset` and `modulemix` [Twig filters](https://twig.symfony.com/doc/3.x/filters/index.html) can be used to generate properly formatted URLs for individual files and files compiled with [Laravel Mix](https://laravel-mix.com), respectively. Required arguments are the module's identifier and the file's name.
```twig
<script src="{{ 'example'|modulemix('/js/app.js') }}"></script>
```

## Schema and migrations
* Database tables are created based on the module's schema in `database/migrations/schema.sql`, if present.
* Migrations are located in `database/migrations/migrations.php` and are structured as follows:
```php
<?php
return [
    [
        'version' => '1.0.1',
        'recreate_tables' => false,
        'sql' => 'ALTER TABLE Foo ADD COLUMN Bar VARCHAR(20);'
    ]
];
```
The version number passed to `Module::registerModule` is used for keeping track of migrations.

::: tip
Migrations are run when the `app/ember.lock` file is present.
:::

## Localization
Translations are read from `resources/lang/en.php` relative to the module's root directory. The file is structured as follows:
```php
<?php
return [
    'category' => [
        'key' => 'value'
    ]
];
```
Translations can be used with the `lang` Twig filter, for example:
```twig
<p>{{ 'key'|lang }}</p>
```

## Module structure
Much like Ember itself, modules consist of **routes**, **controllers** and **views**.

![](../media/screenshots/module_structure.jpg)
