# Overriding files
[Modules](modules) are the recommended way to extend Ember to preserve any customizations when applying new updates. However, if you need to alter the core files it's recommended to use a private [Git](https://git-scm.com) repository to merge updates.

Below are instructions for using some of the tools Ember is built with.

## Twig
Ember uses [Twig](https://twig.symfony.com) for frontend templating. The `.twig` files can be found in `resources/views`. Templates are cached for performance. For changes to be reflected immediately, adjust the last line of `config.php`.
```php
'development_mode' => true
```
After you've finished editing the templates, disable development mode and manually remove the `cache/twig` directory to clear the template cache.


## Vue.js & SCSS
Reactive parts of the frontend are built with [Vue.js](https://vuejs.org) and compiled using [Laravel Mix](https://laravel-mix.com). The source files can be found in `resources/js` and `resources/sass`.

To compile the source files, first you'll need to install [NPM](https://www.npmjs.com/get-npm) and then you'll need to install dependencies using the following command
```
npm install
```
After installing the dependencies, you can build the assets and watch for changes using
```
npm run watch
```
and finally build for production using
```
npm run production
```
