# Discord integration DLC

## Disclaimer
The Discord integration for Ember is not endorsed or created by Discord.

## Preparation
Make sure you have the latest version of [Ember](https://www.gmodstore.com/scripts/view/5620) installed. Download the Discord integration script files from [GmodStore](https://www.gmodstore.com/market/view/6122). Extract the downloaded ZIP archive to a convenient location.

## Uploading the files
Open up the extracted archive. Navigate to the `web` subfolder. Move the **contents** of the subfolder (not the folder itself) to the directory where you installed Ember.
### PHP configuration
Enter a bot URL to the `host` field and a sync token to the `sync_token` field in `web/modules/discord/config.php`. The token will later be assigned to the Discord bot using chat commands.
* when hosting the bot yourself
  * enter the public URL of the bot to the `host` field
    * the format should be similar to `http://<IP>:3000`
  * create your own secure token for the `sync_token` field
    * do not use the one in `shared_sync_token.txt` as it's stored on a backend server
* when using the shared bot
  * enter `https://ember-discord.kekalainen.me` to the `host` field
  * enter the token supplied in `shared_sync_token.txt` to the `sync_token` field

## Adding the bot to a server
### Hosting the bot yourself
To run the bot, you'll need [Node.js](https://nodejs.org/) and [npm](https://www.npmjs.com/get-npm) installed.
- Move the `bot` subfolder to a convenient location.
- Navigate inside of it using the command line and type `npm i` to install the dependencies.
- Set up a [bot application](https://discordapp.com/developers/applications) on Discord and add it to your Discord server.
  - Click on the **New Application** button, enter a name and click on the **Create** button.
  - Select the **Bot** tab from the navigation on the left side of the page.
  - Click on the **Add Bot** button and confirm the bot's creation.
  - Click on the **Copy** button under the **TOKEN** label in the **Build-A-Bot** section and paste the value to the `discord_bot_token` field of the bot's `config.json` file.
  - Scroll down and enable the **SERVER MEMBERS INTENT** under **Privileged Gateway Intents**.
  - Add the bot to your server by navigating to the following URL
    ```
    https://discordapp.com/api/oauth2/authorize?client_id=536911178446667779&scope=bot&permissions=268443648
    ```
    replacing the number after the `client_id` parameter with your bot's **CLIENT ID** found on the **General Information** tab.

After completing the steps above, you can use `node index.js` to run the bot.

### Adding the shared bot
::: danger
By design the shared bot requires permissions to assign, create and remove roles on Discord, and by extension all Ember instances and game servers running Ember it's connected to. In case of a data breach all connected servers are vulnerable. For that reason, **only use the shared bot at your own risk** and preferably host your own instance instead.
:::

If you'd prefer to use the shared bot regardless of the warning above, click [here](https://discordapp.com/api/oauth2/authorize?client_id=536911178446667779&scope=bot&permissions=268443648) to add it to your Discord server.

Once added, Discord should generate a role for the bot automatically. **Make sure to move it to the top of the role hierarchy so that the bot can manage all roles on the server.**

## Configuration
### Bot
The bot can be configured within Discord using chat commands. Use `@Ember#8655 help` to discover the configuration options. Roles can be synced after running `setsynctoken` with the sync token assigned earlier and `seturl` with your Ember installation's URL.
### Module
The module can be configured from **Admin** > **Discord**. Only mapped roles are synced.
