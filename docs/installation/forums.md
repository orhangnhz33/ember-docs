# Forums DLC

## Preparation
Make sure you have the latest version of [Ember](https://www.gmodstore.com/scripts/view/5620) installed. Download the Forums DLC addon files from [GmodStore](https://www.gmodstore.com/market/view/5876). Extract the downloaded ZIP archive to a convenient location.

## Uploading the files
Open up the extracted archive. Navigate to the subfolder containing the `app` and `modules` directories. Merge the directories with the directory in which you installed Ember. Make sure to upload `app/ember.lock` as well.

## Configuration
You should see a **Forums** option added to the **Admin** dropdown in the navigation bar. Using the options there you can configure Forums for Ember to your liking.

There should also be a **Forums** permission category in the [role manager](../configuration/roles-permissions.md#managing-roles). It's recommended to grant the `Everyone` role permissions to post, edit and react to posts.
