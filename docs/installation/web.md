# Web

## Preparation
Download the addon files from [GmodStore](https://www.gmodstore.com/scripts/view/5620). Extract the downloaded ZIP archive to a convenient location.

## Web server configuration
Ember requires a web server capable of performing basic URL rewriting and executing PHP scripts. Configuration examples are provided for Apache, Caddy and NGINX. Note that they'll need to be adjusted for your environment.

### Apache
Make sure that you have [mod_rewrite](https://httpd.apache.org/docs/current/mod/mod_rewrite.html) installed and enabled. Set the [AllowOverride](https://httpd.apache.org/docs/2.4/mod/core.html#allowoverride) configuration directive to `All` to enable parsing the `.htaccess` rules shipped with Ember.

If you're using [PHP-FPM](https://www.php.net/manual/en/install.fpm.php) and certain Apache modules you might need to add `SetEnvIf Authorization "(.*)" HTTP_AUTHORIZATION=$1` to your [virtual host](https://httpd.apache.org/docs/2.4/mod/core.html#virtualhost) configuration.

### Caddy
An example [Caddyfile](https://caddyserver.com/docs/caddyfile) can be found below. Requires [PHP-FPM](https://www.php.net/manual/en/install.fpm.php).
```
example.com {
    root * /var/www/html/public
	php_fastcgi * 127.0.0.1:9000 {
        index index.php
		env SERVER_PORT 443
    }
	rewrite / /index.php?{query}
	file_server
}
```

### NGINX
```
server {
    listen 80;
	server_name example.com;
	root /var/www/html/public;

	location / {
        try_files $uri $uri/ /index.php?$query_string;
    }

    location ~ \.php$ {
        fastcgi_pass 127.0.0.1:9000;
        fastcgi_index index.php;
        fastcgi_param SCRIPT_FILENAME $realpath_root$fastcgi_script_name;
        include fastcgi_params;
    }
    
	error_page 404 /index.php;
}
```

## Setting up a MySQL database
You will need to create a database for Ember as well as a user that has full access to said database. Consult your hosting provider's documentation regarding the creation of those. The database and user can be called anything you'd like. Using the `utf8mb4_general_ci` collation is recommended. Note the IP or domain of your database if it's not running on the same network as your web server.

## Uploading the files
Open up the extracted archive. Navigate to the `web` subfolder. Move the **contents** of the subfolder (not the folder itself) to the **root directory** of your web space. This is often called something along the lines of `html`, `public_html` or `www`. Check with your web host if you can't find the correct directory.

## Configuration file
You should now have the required files uploaded, including the `config.php` file. Open the file for editing and insert your Steam API key (get one from [here](https://steamcommunity.com/dev/apikey)) and the MySQL database configuration you acquired previously. To do that, simply replace the values (right side) of the array elements with your data.

## Initial setup
Browse to the website where you installed Ember using the web browser of your choice. After the setup page is done finishing the installation, sign in with the Steam account using which you purchased this addon. You should see an `Admin` dropdown appear on the navigation bar. Using the options there you can configure Ember to your liking.
