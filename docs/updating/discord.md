# Discord integration DLC

## Disclaimer
The Discord integration for Ember is not endorsed or created by Discord.

## Preparation
Download the new files from [GmodStore](https://www.gmodstore.com/scripts/view/6122). Take a look at the changelog to see if new configuration options have been introduced since the last update. If so, copy the new options into your existing `config.php` and `config.json` file(s).

::: tip
If you're using the [shared bot instance](../installation/discord.md#adding-the-shared-bot), only web files need to be updated.

A new sync token can be found in `shared_sync_token.txt`. It needs to be entered to the `sync_token` field in the module's `config.php` file. Running the `setsynctoken` command again is not necessary.
:::

## Updating from the previous release
If you're only one version behind, simply extract the `update.zip` archive and upload the contents of the extracted `web` and `bot` directories to your web server and bot directory, respectively.

## Updating from an older release
### Removing the old files
Prior to updating, remove old files that you haven't modified to ensure a clean installation. As in, everything but `config.php` and `config.json`.

### Applying the update
* Upload the new files from the `web` directory to your web server. Make sure to upload `web/app/ember.lock` as well.
* Replace the files of your bot with the files from the `bot` directory.
  * Run `npm install` to install updated dependencies.
