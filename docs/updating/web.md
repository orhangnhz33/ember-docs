# Web

## Preparation
Download the new files from [GmodStore](https://www.gmodstore.com/scripts/view/5620). Take a look at `config.php` to see if new settings have been introduced since the last update. If so, copy the new settings into your existing `config.php` file.

## Updating from the previous release
If you're only one version behind, simply extract the `update.zip` archive and upload the contents of the extracted `web` directory to your web server.


## Updating from an older release
### Removing the old files
Prior to updating, remove old files that you haven't modified to ensure a clean installation. As in, everything but `config.php`. The `vendor` directory can be left as is unless dependencies have been updated.


### Applying the update
Upload the new files to your web server. Make sure to upload `app/ember.lock` as well. To finish the update browse to your Ember installation with a web browser. The setup screen will briefly show up and a database update (if necessary) is performed.
